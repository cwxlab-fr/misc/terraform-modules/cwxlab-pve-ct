resource "proxmox_lxc" "cwxlab-pve-ct" {

  target_node  = var.node
  hostname     = var.hostname
  ostemplate   = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  cores        = var.cores
  memory       = var.memory
  swap         = var.swap
  unprivileged = true
  start        = true
  description  = var.description != "" ? var.description : var.hostname

  ssh_public_keys = var.ssh_public_keys

  rootfs {
    storage = "local"
    size    = var.rootfs_size
 }

  mountpoint {
    key     = "0"
    slot    = 0
    storage = "local"
    mp      = var.mountpoint_path
    size    = var.mountpoint_size
  }

  network {
    name     = "eth0"
    bridge   = var.bridge
    ip       = var.ip
    gw       = var.gw
    firewall = false
  }
}
