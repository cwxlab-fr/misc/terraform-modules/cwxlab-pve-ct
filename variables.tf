# TO Test:
#variable "pm_api_url" {
#  type = string
#}
#
#variable "pm_password" {
#  type = string
#}
#
#variable "pm_user" {
#  type = string
#}

variable "node" {
  type        = string
  default     = "node301"
  description = "Target node"
}

variable "hostname" {
  type        = string
  description = "Hostname of the container"
}

variable "cores" {
  type        = number
  default     = 2
  description = "Number of CPU core of the container"
}

variable "memory" {
  type        = number
  default     = 2048
  description = "Quantity of memory for the container"
}

variable "swap" {
  type        = number
  default     = 1024
  description = "Quantity of SWAP for the container"
}

variable "description" {
  type        = string
  default     = ""
  description = "Description of the container, default to hostname"
}

variable "rootfs_size" {
  type        = string
  default     = "8G"
  description = "Size of the root (/) partition"
}

variable "mountpoint_size" {
  type        = string
  default     = "8G"
  description = "Size of the mountpoint partition"
}

variable "mountpoint_path" {
  type        = string
  description = "Path of the mountpoint partition (/opt/some/path)"
}

variable "bridge" {
  type        = string
  description = "Network bridge"
}

variable "ip" {
  type        = string
  description = "IP with CIDR netmask"
}

variable "gw" {
  type        = string
  description = "Gateway"
}

variable "ssh_public_keys" {
  type        = string
  description = "Public keys to add to the containers"
}

